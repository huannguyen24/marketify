import AdminFooter from '@/admin/components/AdminFooter';
import AdminHeader from '@/admin/components/AdminHeader';
import React from 'react';
import { Outlet } from 'react-router-dom';


const AdminLayout: React.FC = () => {
    return (
        <div className="layout">
            <AdminHeader />
            <main className="main-content">
                {/* Render the child routes here */}
                <Outlet />
            </main>
            <AdminFooter />
        </div>
    );
};

export default AdminLayout;