import Footer from '@/user/components/Footer';
import Header from '@/user/components/Header';
import React from 'react';

interface LayoutProps {
    children: React.ReactNode; // The children prop represents the content wrapped by the layout
}

const UserLayout: React.FC<LayoutProps> = ({ children }) => {
    return (
        <div className="layout">
            <Header/>
            <main className="main-content">
                {/* Render the children components passed to the layout */}
                {children}
            </main>
            <Footer/>
        </div>
    );
};

export default UserLayout;