import {
    createBrowserRouter,
} from "react-router-dom";
import ErrorPage from "@/user/components/ErrorPage";
import Home from "@/user/pages/home/Home";
import AdminLayout from "@/layouts/AdminLayout";
import UserLayout from "@/layouts/UserLayout";

export const router = createBrowserRouter([
    {
        path: "/",
        element: <UserLayout>
            <Home />
        </UserLayout>,
        errorElement: <ErrorPage />,
        children: [
        ],
    },
    {
        path: "/admin",
        element: <AdminLayout />,
        children: [
            {
                path: 'dashboard',
                element: <div>Dashboard</div>,
                errorElement: <ErrorPage />,
            }
        ],
    }
]);