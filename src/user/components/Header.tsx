import { ShoppingBag, Zap } from "lucide-react"
import { Link } from "react-router-dom"
import logo from "../../assets/Nextify.svg";
import MainNav from "./MainNav";
import MobileNav from "./MobileNav";

const Header = () => {

  return (
    <>
      <header className="flex w-[700%] sm:w-[500%] md:w-[400%] lg:w-[300%] xl:w-[250%]  head-banner">
        {/* banner */}
        {[1, 1].map((_, ind) => (
          <div key={`${ind}-header`} className="flex-1">
            <ul className="flex list-none py-3 bg-no-repeat bg-cover bg-center justify-around" style={{
              backgroundImage: "url('https://new-ella-demo.myshopify.com/cdn/shop/files/bg-top-bar-promotion-home-6_2048x.png?v=1668151491')"
            }}>
              {[2, 2, 2, 2].map((_, i) => (
                <li key={i} className={`flex flex-row gap-2`}>
                  <Zap className="text-[#fff822]" />
                  <Link to="#" target="_blank" className="text-white">
                    SUMMER SALE: UP TO 70% OFF SELECTED ITEMS
                  </Link>
                </li>
              ))}
            </ul>
          </div>
        ))}
        {/* header */}
      </header >
      <div className='  bg-orange-500 py-4'>
        <div className="container gap-5 flex flex-row items-center justify-between">
          <Link
            className="flex flex-col justify-center items-center gap-1 relative z-10 py-1"
            to="/"
          >
            <ShoppingBag className="size-5 scale-100 transition-all text-white" />
            <img
              src={logo}
              alt="NEXT TIKI for you"
              width={100}
              height={100}
              className="dark:invert cursor-pointer invert-0"
            />
          </Link>
          <div className="md:hidden">
            <MobileNav />
          </div>
          <div className="hidden md:flex flex-1 flex-row items-center">
            <MainNav />
          </div>
        </div>


      </div>
      {/* navbar */}
      <nav className="flex flex-row flex-1 gap-5 justify-between">

        <div className="flex-wrap flex gap-5">
          {/* {searchCategories.map(({ name, id, link }: ISearchCategories) => (
            <Link key={id} href={link}>
              <span className="cursor-pointer text-gray-400 text-sm">
                {name}
              </span>
            </Link>
          ))} */}
          asdsad
        </div>
        {/* <LocationPointer /> */}
      </nav>
    </>
  )
}

export default Header