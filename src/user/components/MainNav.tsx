import SearchInput from './SearchInput'
import { Button } from '@/components/ui/button'
import { useNavigate } from 'react-router-dom'
import { DropdownLanguagesMenu } from './LangDropDown'
import DarkModeToggle from './DarkModeToggle'
import UserController from './UserController'
import { Separator } from '@/components/ui/separator'
import ShoppingCarts from './ShoppingCarts'

const MainNav = () => {
    const navigate = useNavigate()
    return (
        <>
            {/* DARK MODE THEME, LANGS AND SEARCH ...*/}
            <div className="w-full">
                <SearchInput />
            </div>
            <div className="flex space-x-2 items-center">
                <Button variant="ghost" onClick={() => navigate("/")}>
                    Trang chủ
                </Button>
                <DropdownLanguagesMenu />
                <DarkModeToggle />
                <UserController />
            </div>
            <Separator orientation="vertical" />
            <ShoppingCarts />
        </>
    )
}

export default MainNav